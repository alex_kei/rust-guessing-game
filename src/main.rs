use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    const MIN: u8 = 1;
    const MAX: u8 = 100;
    let secret_number: u8 = rand::thread_rng()
        .gen_range(1..101);

    let mut tries_number: u8 = 0;

    println!("Guess the number between {} and {}!", MIN, MAX);

    loop {
        tries_number += 1;

        println!("Please input your guess:");

        let mut guess_input = String::new();

        io::stdin()
            .read_line(&mut guess_input)
            .expect("Failed to read line");
        
        let guess_number: u8 = match guess_input.trim().parse() {
            Ok(number) => number,
            Err(_) => {
                println!("Use only integer number between {} and {}.", MIN, MAX);
                continue;
            }
        };

        match guess_number.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }

    println!("You used {} tries", tries_number);
}
