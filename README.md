# Simple usage

## Build progect

Run in console:

```
cargo build
```

Then yo can check for executable in `./target/debug`.
It is `./target/debug/guessing_game`.


## Run program

To run program you can use built executable (see previous section)
or you can run in console:

```
cargo run
```